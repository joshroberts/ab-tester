import Vapor
import HTTP

final class ABTestController: ResourceRepresentable {
    
    // MARK: GET /ab-tests
    
    func index(_ req: Request) throws -> ResponseRepresentable {
        return try ABTest.all().makeJSON()
    }
    
    // MARK: GET /ab-tests/abTest-id
    
    func show(_ req: Request, abTest: ABTest) throws -> ResponseRepresentable {
        return abTest
    }
    
    // MARK: POST /ab-tests
    
    func store(_ req: Request) throws -> ResponseRepresentable {
        let abTest = try req.abTest()
        try abTest.save()
        let options = try req.options()
        try options.forEach {
            $0.testId = abTest.id
            try abTest.options.save($0)
        }
        return abTest
    }
    
    // MARK: PATCH /ab-tests/abTest-id
    
    func update(_ req: Request, abTest: ABTest) throws -> ResponseRepresentable {
        try abTest.update(for: req)
        try abTest.save()
        return abTest
    }
    
    // MARK: ResourceRepresentable
    
    func makeResource() -> Resource<ABTest> {
        return Resource(
            index: index,
            store: store,
            show: show,
            update: update,
            replace: nil,
            destroy: nil,
            clear: nil
        )
    }
}

// MARK: - Request init helper

fileprivate extension Request {
    
    func abTest() throws -> ABTest {
        guard let json = json else { throw Abort.badRequest }
        return try ABTest(json: json)
    }
    
    func options() throws -> [TestValue] {
        guard let optionsJson: JSON = try json?.get(ABTest.Keys.options), let array = optionsJson.array else { throw Abort.badRequest }
        return try array.map { try TestValue(json: $0) }
    }
}

// MARK: - EmptyInitializable

extension ABTestController: EmptyInitializable { }
