import Vapor
import HTTP

final class DeviceController: ResourceRepresentable {

    // MARK: GET /devices
    
    func index(_ req: Request) throws -> ResponseRepresentable {
        return try Device.all().makeJSON()
    }
    
    // MARK: GET /devices/device-id
    
    func show(_ req: Request, device: Device) throws -> ResponseRepresentable {
        return device
    }
    
    // MARK: POST /devices
    
    func store(_ req: Request) throws -> ResponseRepresentable {
        let device = try req.device()
        try device.save()
        return device
    }
    
    // MARK: PATCH /devices/device-id
    
    func update(_ req: Request, device: Device) throws -> ResponseRepresentable {
        try device.update(for: req)
        try device.save()
        return device
    }
    
    // MARK: ResourceRepresentable
    
    func makeResource() -> Resource<Device> {
        return Resource(
            index: index,
            store: store,
            show: show,
            update: update,
            replace: nil,
            destroy: nil,
            clear: nil
        )
    }
}

// MARK: - Request init helper

fileprivate extension Request {
    
    func device() throws -> Device {
        guard let json = json else { throw Abort.badRequest }
        return try Device(json: json)
    }
}

// MARK: - EmptyInitializable

extension DeviceController: EmptyInitializable { }
