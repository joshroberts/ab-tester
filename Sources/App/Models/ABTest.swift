import Vapor
import FluentProvider
import HTTP
import Foundation

final class ABTest: Model {
    let storage = Storage()
    
    // Enums
    
    enum ValueType: String {
        case string = "STRING"
        case color = "COLOR"
        case int = "INT"
        case double = "DOUBLE"
        case float = "FLOAT"
    }
    
    // MARK: Properties
    
    var description: String
    var type: String
    var expirationTime: Date
    var defaultIndex: Int
    
    var options: Children<ABTest, TestValue> {
        return children()
    }
    
    // MARK: Database keys
    
    struct Keys {
        static let id = "id"
        static let type = "type"
        static let description = "description"
        static let expirationTime = "expirationTime"
        static let defaultIndex = "defaultIndex"
        static let options = "options"
    }
    
    // MARK: init
    
    init(description: String, type: String, expirationTime: String, defaultIndex: Int) {
        self.description = description
        self.type = type //ValueType(rawValue: type) ?? .string
        self.expirationTime = DateFormatter.iso8601.date(from: expirationTime) ?? Date()
        self.defaultIndex = defaultIndex
    }
    
    // MARK: Fluent Serialization
    
    init(row: Row) throws {
        description = try row.get(Keys.description)
        type = try row.get(Keys.type)
//        if let rawType: String = try? row.get(Keys.type) {
//            type = ValueType(rawValue: rawType) ?? .string
//        } else {
//            type = .string
//        }
        expirationTime = try row.get(Keys.expirationTime)
        defaultIndex = try row.get(Keys.defaultIndex)
    }
    
    func makeRow() throws -> Row {
        var row = Row()
        try row.set(Keys.description, description)
        try row.set(Keys.type, type)
        try row.set(Keys.expirationTime, expirationTime)
        try row.set(Keys.defaultIndex, defaultIndex)
        return row
    }
}

// MARK: Fluent Preparation

extension ABTest: Preparation {
    
    static func prepare(_ database: Database) throws {
        try database.create(self) { builder in
            builder.id()
            builder.string(ABTest.Keys.description)
            builder.string(ABTest.Keys.type)
            builder.string(ABTest.Keys.expirationTime)
            builder.string(ABTest.Keys.defaultIndex)
        }
    }
    
    static func revert(_ database: Database) throws {
        try database.delete(self)
    }
}

// MARK: JSON

extension ABTest: JSONConvertible {
    convenience init(json: JSON) throws {
        self.init(
            description: try json.get(ABTest.Keys.description),
            type: try json.get(ABTest.Keys.type),
            expirationTime: try json.get(ABTest.Keys.expirationTime),
            defaultIndex: try json.get(ABTest.Keys.defaultIndex)
        )
    }
    
    func makeJSON() throws -> JSON {
        var json = JSON()
        try json.set(ABTest.Keys.description, description)
        try json.set(ABTest.Keys.type, type)
        try json.set(ABTest.Keys.expirationTime, expirationTime)
        try json.set(ABTest.Keys.defaultIndex, defaultIndex)
        try json.set(ABTest.Keys.options, options.all())
        return json
    }
}

// MARK: HTTP

extension ABTest: ResponseRepresentable { }

// MARK: Update

extension ABTest: Updateable {
    
    public static var updateableKeys: [UpdateableKey<ABTest>] {
        return [
//            UpdateableKey(ABTest.Keys.model, String.self) { device, model in device.model = model },
//            UpdateableKey(ABTest.Keys.osVersion, String.self) { device, osVersion in device.osVersion = osVersion },
//            UpdateableKey(ABTest.Keys.appVersion, String.self) { device, appVersion in device.appVersion = appVersion }
        ]
    }
}
