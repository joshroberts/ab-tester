import Vapor
import FluentProvider
import HTTP

final class Device: Model {
    let storage = Storage()
    
    // MARK: Properties
    
    var uuid: String
    var description: String
    var model: String
    var osVersion: String
    var appVersion: String
    
    // MARK: Database keys
    
    struct Keys {
        static let id = "id"
        static let uuid = "uuid"
        static let description = "description"
        static let model = "model"
        static let osVersion = "osVersion"
        static let appVersion = "appVersion"
    }

    // MARK: init
    
    init(uuid: String, description: String, model: String, osVersion: String, appVersion: String) {
        self.uuid = uuid
        self.description = description
        self.model = model
        self.osVersion = osVersion
        self.appVersion = appVersion
    }

    // MARK: Fluent Serialization

    init(row: Row) throws {
        uuid = try row.get(Keys.uuid)
        description = try row.get(Keys.description)
        model = try row.get(Keys.model)
        osVersion = try row.get(Keys.osVersion)
        appVersion = try row.get(Keys.appVersion)
    }

    func makeRow() throws -> Row {
        var row = Row()
        try row.set(Keys.uuid, uuid)
        try row.set(Keys.description, description)
        try row.set(Keys.model, model)
        try row.set(Keys.osVersion, osVersion)
        try row.set(Keys.appVersion, appVersion)
        return row
    }
}

// MARK: Fluent Preparation

extension Device: Preparation {
    
    static func prepare(_ database: Database) throws {
        try database.create(self) { builder in
            builder.id()
            builder.string(Device.Keys.uuid)
            builder.string(Device.Keys.description)
            builder.string(Device.Keys.model)
            builder.string(Device.Keys.osVersion)
            builder.string(Device.Keys.appVersion)
        }
    }

    static func revert(_ database: Database) throws {
        try database.delete(self)
    }
}

// MARK: JSON

extension Device: JSONConvertible {
    convenience init(json: JSON) throws {
        self.init(
            uuid: try json.get(Device.Keys.uuid),
            description: try json.get(Device.Keys.description),
            model: try json.get(Device.Keys.model),
            osVersion: try json.get(Device.Keys.osVersion),
            appVersion: try json.get(Device.Keys.appVersion)
        )
    }
    
    func makeJSON() throws -> JSON {
        var json = JSON()
        try json.set(Device.Keys.uuid, uuid)
        try json.set(Device.Keys.description, description)
        try json.set(Device.Keys.model, model)
        try json.set(Device.Keys.osVersion, osVersion)
        try json.set(Device.Keys.appVersion, appVersion)
        return json
    }
}

// MARK: HTTP

extension Device: ResponseRepresentable { }

// MARK: Update

extension Device: Updateable {
    
    public static var updateableKeys: [UpdateableKey<Device>] {
        return [
            UpdateableKey(Device.Keys.description, String.self) { device, description in device.description = description },
            UpdateableKey(Device.Keys.model, String.self) { device, model in device.model = model },
            UpdateableKey(Device.Keys.osVersion, String.self) { device, osVersion in device.osVersion = osVersion },
            UpdateableKey(Device.Keys.appVersion, String.self) { device, appVersion in device.appVersion = appVersion }
        ]
    }
}
