import Vapor
import FluentProvider
import HTTP

final class TestValue: Model {
    let storage = Storage()
    
    // MARK: Properties
    
    var testId: Identifier?
    var value: String
    var splitRatio: Double
    
    var test: Parent<TestValue, ABTest> {
        return parent(id: testId)
    }
    
    // MARK: Database keys
    
    struct Keys {
        static let value = "value"
        static let splitRatio = "splitRatio"
    }
    
    // MARK: init
    
    init(testId: Identifier? = nil, value: String, splitRatio: Double) {
        self.testId = testId
        self.value = value
        self.splitRatio = splitRatio
    }
    
    // MARK: Fluent Serialization
    
    init(row: Row) throws {
        testId = try row.get(ABTest.foreignIdKey)
        value = try row.get(Keys.value)
        splitRatio = try row.get(Keys.splitRatio)
    }
    
    func makeRow() throws -> Row {
        var row = Row()
        try row.set(ABTest.foreignIdKey, testId)
        try row.set(Keys.value, value)
        try row.set(Keys.splitRatio, splitRatio)
        return row
    }
}

// MARK: Fluent Preparation

extension TestValue: Preparation {
    
    static func prepare(_ database: Database) throws {
        try database.create(self) { builder in
            builder.id()
            builder.parent(ABTest.self)
            builder.string(TestValue.Keys.value)
            builder.double(TestValue.Keys.splitRatio)
        }
    }
    
    static func revert(_ database: Database) throws {
        try database.delete(self)
    }
}

// MARK: JSON

extension TestValue: JSONConvertible {
    convenience init(json: JSON) throws {
        self.init(
            value: try json.get(TestValue.Keys.value),
            splitRatio: try json.get(TestValue.Keys.splitRatio)
        )
    }
    
    func makeJSON() throws -> JSON {
        var json = JSON()
        try json.set(TestValue.Keys.value, value)
        try json.set(TestValue.Keys.splitRatio, splitRatio)
        return json
    }
}

// MARK: HTTP

extension TestValue: ResponseRepresentable { }
