import Vapor

extension Droplet {
    func setupRoutes() throws {

        // response to requests to /info domain
        // with a description of the request
        get("info") { req in
            return req.description
        }
        
        try resource("devices", DeviceController.self)
        try resource("ab-tests", ABTestController.self)
    }
}
