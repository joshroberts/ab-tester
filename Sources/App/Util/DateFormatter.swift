import Foundation

extension DateFormatter {
    
    fileprivate class func formatter(for format: String) -> DateFormatter {
        let formatter = DateFormatter()
        formatter.locale = Locale(identifier: "en_GB")
        formatter.dateFormat = format
        return formatter
    }
    
    static let iso8601: DateFormatter = {
        return DateFormatter.formatter(for: "yyyy-MM-dd'T'HH:mm:ss")
    }()

}
